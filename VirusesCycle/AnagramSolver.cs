﻿using System;
namespace VirusesCycle
{
    public class AnagramSolver
    {

            public static void Main(string[] args){
                //System.IO.StreamReader sr;
                //sr = new System.IO.StreamReader(args[0]);
                string[] lines = new string[2];
                string line = null;
                string a = null, b = null;
                int i = 0;
                int p = 0;
                try{
               
                    a = "eyssaasse";
                    b = "essayasse";
                    /* YOUR  CODE HERE */
                    if (a.Equals(b)) {
                        Console.WriteLine("{0}", 0);
                    }
                    else if (a.Length != b.Length) {
                        Console.WriteLine("{0}", -1);
                    } else {
                        var aArray = a.ToLower().ToCharArray();
                        var bArray = b.ToLower().ToCharArray();

                        Array.Sort(aArray);
                        Array.Sort(bArray);

                        string s1 = new string(aArray);
                        string s2 = new string(bArray);

                        if(!s1.Equals(s2)) {
                            Console.WriteLine("{0}", -1);
                        } else {
                            for (int k = 0; k < a.Length; k++)
                            {
                                if (a[k] == b[k]) {
                                    continue;
                                } else {
                                    Char temp = b[k];
                                    b.Replace(b[k],b[k+1]);
                                    b.Replace(b[k+1],temp);
                                    p++;
                                }
                            }                
                            if (true)
                                Console.WriteLine(p + " " + a + " " + b);
                        }
                    }
                }
                catch (Exception e){
                    System.Diagnostics.Trace.WriteLine(e.Message);
                }
            }
        }
    }
