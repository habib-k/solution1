﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;

namespace viruses_life_cycle
{
	public class VirusesLifeCycle
	{
		public static Dictionary<Tuple<int, int>, int> HashTable;
		public static bool CalledFromTest = false;
		public static int M { get; set; }
		public static int N { get; set; }
		public static int K { get; set; }
		public static int[,] A { get; set; }

		public static void Main(string[] args)
		{

			A = PopulateInitialMatrix(args);

			M = GetM();
			N = GetN();
			K = GetK();
			int[,] result = new int[M, N];
			for (int i = 0; i < M; i++)
			for (int j = 0; j < N; j++)
				result[i, j] = 1;

			/* YOUR CODE HERE */

			List<int[,]> lstMatrices = new List<int[,]>();
			lstMatrices.Add(A);

			for (int k = 0; k < K; k++)
			{
				int[,] Aa = new int[M, N];
				int[,] Ab = new int[M, N];

				Aa = (int[,]) lstMatrices[k].Clone();
				Ab = (int[,]) lstMatrices[k].Clone();

				for (int i = 0; i < M; i++)
				{
					for (int j = 0; j < N; j++)
					{
						if (Ab[i, j] == 1)
						{
							regleUn(Aa, Ab, i, j, M, N);
						}
					}
				}

				if (k > 3)
				{
					for (int i = 0; i < M; i++)
					{
						for (int j = 0; j < N; j++)
						{
							regleDeux(lstMatrices, Aa, i, j);
						}
					}
				}

				Ab = (int[,]) Aa.Clone();
				for (int i = 0; i < M; i++)
				{
					for (int j = 0; j < N; j++)
					{
						if (Aa[i, j] == 0)
						{
							regleTrois(Aa, Ab, i, j, M, N);
						}
					}
				}

				lstMatrices.Add(Aa);
			}

			result = lstMatrices[K];

			Console.WriteLine("debut");
			for (int i = 0; i < M; i++)
			{
				for (int j = 0; j < N; j++)
				{
					if (j != N - 1)
					{
						Console.Write("{0}", result[i, j]);
						Console.Write(" ");
					}
					else
					{
						Console.Write("{0}", result[i, j]);
						Console.WriteLine();
					}
				}
			}

		}

		public static int[,] PopulateInitialMatrix(string[] args)
		{
			HashTable = new Dictionary<Tuple<int, int>, int>();
			if (args.Count() == 1)
			{
				var sr = new System.IO.StreamReader(args[0]);

				List<string> lines = new List<string>();
				string line;

				while ((line = sr.ReadLine()) != null)
					lines.AddRange(line.Split(' '));

				int h = 0;
				string[] matrix = new string[lines.Count - 3];
				for (int j = 3; j < lines.Count; j++)
					matrix[h++] = lines[j];

				M = int.Parse(lines[0]);
				N = int.Parse(lines[1]);
				K = int.Parse(lines[2]);

				A = new int[M, N];
				h = 0;
				for (var i = 0; i < M; i++)
				for (var j = 0; j < N; j++)
					A[i, j] = int.Parse(matrix[h++]);
			}

			return A;
		}

		private static int GetM()
		{
			return M;
		}

		private static int GetN()
		{
			return N;
		}

		private static int GetK()
		{
			return K;
		}

		public static void regleUn(int[,] m1, int[,] m2, int x, int y, int m, int n)
		{
			int somme = 0;
			if (x == 0)
			{
				if (y == 0)
				{
					somme = m2[x, y + 1] + m2[x + 1, y] + m2[x + 1, y + 1];

				}
				else if (y == n - 1)
				{
					somme = m2[x, y - 1] + m2[x + 1, y - 1] + m2[x + 1, y];

				}
				else
				{
					somme = m2[x, y - 1] + m2[x, y + 1] + m2[x + 1, y - 1] + m2[x + 1, y] + m2[x + 1, y + 1];
				}
			}
			else if (x == m - 1)
			{
				if (y == 0)
				{
					somme = m2[x - 1, y] + m2[x - 1, y + 1] + m2[x, y + 1];

				}
				else if (y == n - 1)
				{
					somme = m2[x - 1, y - 1] + m2[x - 1, y] + m2[x, y - 1];

				}
				else
				{
					somme = m2[x - 1, y - 1] + m2[x - 1, y] + m2[x - 1, y + 1] + m2[x, y - 1] + m2[x, y + 1];

				}
			}
			else
			{
				if (y == 0)
				{
					somme = m2[x - 1, y] + m2[x - 1, y + 1] + m2[x, y + 1] + m2[x + 1, y] + m2[x + 1, y + 1];

				}
				else if (y == n - 1)
				{
					somme = m2[x - 1, y - 1] + m2[x - 1, y] + m2[x, y - 1] + m2[x + 1, y - 1] + m2[x + 1, y];

				}
				else
				{
					somme = m2[x - 1, y - 1] + m2[x - 1, y] + m2[x - 1, y + 1] + m2[x, y - 1] +
					        m2[x, y + 1] + m2[x + 1, y - 1] + m2[x + 1, y] + m2[x + 1, y + 1];

				}
			}

			if (somme < 2 || somme > 3)
			{
				m1[x, y] = 0;
			}
		}

		public static void regleDeux(List<int[,]> lstMatrices, int[,] matrix, int x, int y)
		{
			int virus = 0;
			int count = lstMatrices.Count;
			int itra = count - 3;
			for (int i = itra; i < count; i++)
			{
				matrix = lstMatrices[i];
				if (matrix[x, y] == 1)
				{
					virus++;
				}
			}

			if (virus == 3)
			{
				matrix[x, y] = 1;
			}
		}

		public static void regleTrois(int[,] m1, int[,] m2, int x, int y, int m, int n)
		{
			if (y > 0)
			{
				if (m2[x, y - 1] == 1)
				{
					m1[x, y] = 1;
				}
			}



		}
	}
}